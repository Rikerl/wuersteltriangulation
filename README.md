# LIEBER JASON !

Um diesen Rätsel zu lösen, musst du:

- diesen Git-Repo auschecken
- deine lieblings python environment aktivieren
- den jupyter notebook "Wuerstel_triangulation.ipynb" starten

Viel Spaß... und alles Gute zum Geburtstag!

Bussi,

deine Geek-Freunde